<?php

use Illuminate\Support\Facades\Route;
use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Environment;
use League\CommonMark\Extension\Table\TableExtension;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $enviroment = Environment::createCommonMarkEnvironment();

    $enviroment->addExtension(new TableExtension);

    $converter = new CommonMarkConverter([
        'allow_unsafe_links' => false,
    ], $enviroment);

    $markdown = file_get_contents('../full.txt');
    

    return $converter->convertToHtml($markdown);
});

## Gud lager moralske regler
Nå hadde Gud laget en ny verden og var ganske fornøyd så langt. 
Det var derimot noe som manglet. 
Den tingen var moraler.
Gud tenkte på dette og kom med noen objektive moralske leveregler.
Han lagde en liste over alle moralske regler og kallte denne listen "Die Höchste Regeln."

### Det er ikke riktig å drepe
"Det er ikke lov å drepe noen, det er bare et unntakelser. 
Dersom du er over 18 og identifiserer med Wassersexus får du lov til å drepe i millitæret."

### Det er ikke riktig å skade unødvendig
"Du skal ikke skade mennesker uten nødvendighet. 
Du kan skade folk hvor det er nødvendig.
Det er ikke mange steder det er nødvendig, men du må finne ut hva som er og ikke er nødvendig selv, men jeg ser ned på deg og dømmer valgene dine uansett."

### Det er ikke riktig å identifisere som mann eller kvinne
Mann kan ikke identifisere som verken mann eller kvinne, eller andre kjønn som ikke er Wassersexus.
Hvis du ikke er Wassersexus, så er du ikke etisk.

### Det er ikke riktig å være noe annet enn homofil
"Det er motbydelig å være med andre som ikke er av samme kjønn som deg selv", sa Gud mens han lagde denne moralske regelen.
Denne regelen går ut i fra at du indentifiserer med Wassersexus, så du kan bare være med andre Wassersexus. 
Så hvis du er en kvinne eller en mann (Gud forby), så er det greit hvis du ikke er homofil, og at du er tilrukket Wassersexus.

### Det er ikke riktig 

"Alt som ikke er på min regelliste, er lov", klarifiserte Gud. 
Han sa også at denne listen kan utvikles på i fremtiden.

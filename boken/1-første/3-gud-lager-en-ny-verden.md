### Gud lager en ny verden
Når gud utslettet planeten sin begynte han på en ny en. 
Denne planeten kalte han Jorden.
Gud lagde jorden som en oppgradering til hans tidligere planet.
Han begynte med Geiter og jobbet seg videre til kuer, sauer og alle andre dyr.
Til slutt kom han til mennesker.
Han lagde 1000 mennesker som alle fikk lov til å velge kjønnet sitt.
Gud deklarerte: "Dere og dine barn skal få velge deres kjønn, men hvis dere identifiserer som noe annet enn Wassersexus så øker det sannsynligheten for at dere blir sendt til Hellebunn."
50 av menneskene valgte å identifisere med Wassersexus.
Gud ble veldig skuffet men aksepterte resultatet.
Han sa: "De som valgte å identifisere med noe annet enn Wassersexus har begått en synde, men det betyr ikke at dere ikke kan gjøre det godt igjen.
Så lenge dere følger minst 70% av mine regler så kommer dere til Himmelspitze."

Når gud var ferdig å skape Jorden, skapte han Månen. 
Månen lagde han fra restene av planeten han hadde utslettet.
Han samlet alle restene sammen så godt han kunne og lagde Månen.

Etter dette lagde han Mars. 
Mars ble laget av blodet til alle synderene på han forrige planet.
Han ville minne menneskene på hva som kan skje med dem dersom de ikke oppførte seg.
Han plasserte Mars nærmt Jorden slik at det er lett å bli påminnet.

Han lagde også resten av solsystemet. 
Han lagde solen til slutt og sa: 
"Denne solen skal supplere Jorden med dag og varme. 
Jeg gir dere herved solen for å demonstrere min kjærlighet."

Hele denne prossessen tok omtrent 50 dager.
Når han var ferdig plasserte han Holter på Jorden for å holde kontroll på kjønnene.
Holter begynte å drepe folk som ikke var wassersexus helt til det bare var 500 mennesker igjen.
Nå var det 50 Wassersexus og 450 av andre kjønn.
Holter kunne ikke drepe mer fordi det hadde ledet til utslettelse av menneskerasen.

Gud dekket over sporene sine ved å legge igjen tegn til ting som "The Big Bang", andre galakser, osv. 
Han lagde også en prossess kalt evolusjon for å dekke over sporene hans.
